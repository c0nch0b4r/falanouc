=========
Changelog
=========

Version 0.1.0 (10/22/2020)
--------------------------
* First public commit

Version 0.1.1 (10/22/2020)
--------------------------
* Added ``DiscordWebHookNotifier``

Version 0.1.2 (10/22/2020)
--------------------------
* Bugfixes
* falanouc now properly copies an example config on first run

Version 0.1.3 (04/25/2021)
--------------------------
* Added pushbullet support

Version 0.1.3 (05/12/2021)
--------------------------
* Added SD_NOTIFY support
* Fixed improper handling of pushbullet attributes
* Made adding additional matchers easier
* Added regex matcher
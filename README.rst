======
README
======

Falanouc is a small library and script created to monitor FurAffinity artists' submissions for
certain keywords and to alert on matches. It can either be run periodically as a one-off, or
run in daemon mode to continually monitor for updates.


-----
Setup
-----

If a config file is not specified and one does not exist in the default location, an example config
file will be copied to ``~/.config/falanouc/config.json``. Be sure that you provide values for
``cookie_a`` and ``cookie_b`` (for more information about how to get these values, see the
`FAAPI readme`_.


-------------
Configuration
-------------

Configuration is done through a json file with the following possible values:
 - db_path
    Path to the sqlite database file. If the file doesn't exist, it will be created.
 - idle_timer
    Amount of time in seconds between runs when operating as a daemon
 - cookie_a, cookie_b
    Cookies from a valid FurAffinity.net session, required for many calls.
 - notifiers
    Dict of classes resolvable with ``pydoc.locate``, with the keys being either a list of
    positional arguments or a dict of keyword arguments for class instantiation. The class must be
    an instance of ``falanouc.notifiers.NotifierBase``, and have a ``main()`` function that accepts
    two variables -- the first a ``FalanoucSubmission`` and the second a ``FalanoucMatchBundle``.
 - artists
    Dict of artists with their username on FurAffinity as the key and search parameters as the
    value. See ``falanouc/data/config.json.example`` for the correct structure.

-----
Usage
-----

Falanoc can be invoked as a script to be run in a one-off fashion, can be run as a daemon, or can
be imported and used in python scripts as a library.

Running as a one-off
********************

::

  $ falanouc -d
  2020-10-22 01:28:26,748 [INFO   ] Falanouc started
  2020-10-22 01:28:26,748 [DEBUG  ] Loading notifier `falanouc.notifiers.LogfileNotifier`
  2020-10-22 01:28:26,756 [DEBUG  ] Starting new HTTPS connection (1): www.furaffinity.net:443
  2020-10-22 01:28:26,870 [DEBUG  ] https://www.furaffinity.net:443 "GET / HTTP/1.1" 200 None
  2020-10-22 01:28:26,876 [DEBUG  ] Starting new HTTPS connection (1): www.furaffinity.net:443
  2020-10-22 01:28:26,971 [DEBUG  ] https://www.furaffinity.net:443 "GET /robots.txt HTTP/1.1" 200 None
  2020-10-22 01:28:26,975 [INFO   ] Loading previously seen submissions from the database
  2020-10-22 01:28:26,978 [INFO   ] Checking for new submissions
  2020-10-22 01:28:26,978 [INFO   ] Checking `fiuefey` (1/1)
  2020-10-22 01:28:26,984 [DEBUG  ] Starting new HTTPS connection (1): www.furaffinity.net:443
  2020-10-22 01:28:27,236 [DEBUG  ] https://www.furaffinity.net:443 "GET /user/fiuefey HTTP/1.1" 200 None
  2020-10-22 01:28:27,289 [DEBUG  ] Artist `fiuefey` : `0`
  2020-10-22 01:28:28,058 [DEBUG  ] https://www.furaffinity.net:443 "GET /gallery/fiuefey/1 HTTP/1.1" 200 None
  2020-10-22 01:28:28,108 [INFO   ] Found 0 new submissions.


Importing as a Library
**********************

>>> from falanouc import FalanoucRunner, FalanoucConfig
>>> from falanouc.notifiers import LogfileNotifier
>>> import logging
>>> logging.basicConfig(level=logging.INFO)
>>> notifier = LogfileNotifier()
>>> config = FalanoucConfig.from_json('config.json')
>>> falanouc = FalanoucRunner(config=config, submission_notifiers=[notifier])
>>> falanouc.main()
INFO:falanouc.classes.FalanoucRunner:Loading previously seen submissions from the database
INFO:falanouc.classes.FalanoucRunner:Checking for new submissions
INFO:falanouc.classes.FalanoucRunner:Checking `fiuefey` (1/1)
INFO:falanouc.classes.FalanoucRunner:Found matching submission `37271990`
INFO:falanouc.classes.FalanoucRunner:Found matching submission `37271863`
INFO:falanouc.classes.FalanoucRunner:Found matching submission `36546628`
...
INFO:falanouc.classes.FalanoucRunner:Found matching submission `34570155`
INFO:falanouc.classes.FalanoucRunner:Found 72 new submissions.

-----
To-Do
-----

 - Create task to periodically prune old entries from the database to prevent bloat
    - Maybe use a bloom filter for space efficiency
 - More options for filtering and matching
 - Have falanouc run through a setup wizard at first run
 - Support for adding artists and tags/keywords via script
 - Support downloading matching art
 - Support for journals
 - Support for filtering on art type
 - Fix SIGHUP handling
 - Move artists and keywords to DB
 - Support for multiple notifiers of the same type
 - Support for selecting which notifiers to include via extras
 - Add more possible matchers (fuzzy matching, keyword density, etc.)


----------------
Acknowledgements
----------------

This project wouldn't be possible without FAAPI_

.. _FAAPI: https://gitlab.com/MatteoCampinoti94/FAAPI
.. _`FAAPI readme`: https://gitlab.com/MatteoCampinoti94/FAAPI/-/blob/master/README.md#cookies
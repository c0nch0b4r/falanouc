#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import Any, List, Optional

import faapi
import re

from falanouc.mixins import LoggerMixin


class FalanoucMatch(object):
    def __init__(
            self,
            match_type: str,
            matched_on: str,
            matcher: str
    ) -> None:
        self.match_type = match_type
        self.matched_on = matched_on
        self.matcher = matcher

    def __str__(self) -> str:
        if self.matched_on == '*':
            return f'{self.match_type} matched wildcard'
        return f'{self.match_type} matched from {self.matcher} on `{self.matched_on}`'

    def __add__(self, other):
        if isinstance(other, FalanoucMatch):
            return FalanoucMatchBundle(matches=[self, other])
        if isinstance(other, FalanoucMatchBundle):
            # noinspection PyTypeChecker
            return other + self


class FalanoucMatchBundle(object):
    def __init__(
            self,
            matches: Optional[List[FalanoucMatch]] = None
    ) -> None:
        if not matches:
            matches = []
        self.matches = matches
        self._iter_matches = []

    def __str__(self) -> str:
        match_strings = []
        for match in self.matches:
            match_strings.append(str(match))
        return ', '.join(match_strings)

    def __bool__(self) -> bool:
        if self.matches:
            return True
        return False

    def __add__(self, other):
        self_set = set(self.matches)
        other_set = set(other.matches)
        self_set.update(other_set)
        self.matches = list(self_set)
        return self

    def __sub__(self, other):
        assert(isinstance(other, FalanoucMatch))
        if other in self:
            self.matches.remove(other)
        return self

    def __len__(self):
        return len(self.matches)

    def __contains__(self, item):
        if item in self.matches:
            return True
        return False

    def __iter__(self):
        self._iter_matches = self.matches.copy()
        return self

    def __next__(self):
        if len(self._iter_matches) >= 1:
            return self._iter_matches.pop()
        else:
            raise StopIteration

    def add_match(
            self,
            match: FalanoucMatch
    ) -> None:
        if match in self.matches:
            return
        self.matches.append(match)


class FalanoucMatcher(LoggerMixin, object):
    def __init__(
            self,
            submission: faapi.submission,
            search_key: Any
    ) -> None:
        self.submission = submission
        self.search_key = search_key

    def matches(self) -> Optional[FalanoucMatchBundle]:
        raise NotImplementedError


class FalanoucTextMatcher(FalanoucMatcher):
    def __init__(self, submission: faapi.submission, search_key: str) -> None:
        super().__init__(submission, search_key)
        self.search_key = search_key.lower()

    def matches(self) -> Optional[FalanoucMatchBundle]:
        match_bundle = FalanoucMatchBundle()
        if self.search_key == '*':
            match_bundle.add_match(
                FalanoucMatch(match_type='wildcard', matched_on='*', matcher='text')
            )
            self.logger.debug(f'Submission `{self.submission.id}` matched wildcard')
            return match_bundle
        elif self.search_key in self.submission.description.lower():
            match_bundle.add_match(
                FalanoucMatch(match_type='description', matched_on=self.search_key, matcher='text')
            )
            self.logger.debug(
                f'Submission `{self.submission.id}` description matched keyword `{self.search_key}`'
            )
        elif self.search_key in self.submission.title.lower():
            match_bundle.add_match(
                FalanoucMatch(match_type='title', matched_on=self.search_key, matcher='text')
            )
            self.logger.debug(
                f'Submission `{self.submission.id}` title matched keyword `{self.search_key}`'
            )
        for tag in self.submission.tags:
            if self.search_key in tag.lower():
                match_bundle.add_match(
                    FalanoucMatch(match_type='tag', matched_on=tag.lower(), matcher='text')
                )
                self.logger.debug(
                    f'Submission `{self.submission.id}` tag `{tag}` matched keyword '
                    f'`{self.search_key}`'
                )
        if match_bundle:
            return match_bundle
        return


class FalanoucRegexMatcher(FalanoucMatcher):
    def __init__(self, submission: faapi.submission, search_key: str) -> None:
        super().__init__(submission, search_key)
        self.regex = re.compile(self.search_key)

    def matches(self) -> Optional[FalanoucMatchBundle]:
        match_bundle = FalanoucMatchBundle()
        description_matches = re.search(self.regex, self.submission.description)
        title_matches = re.search(self.regex, self.submission.title)
        tag_matches = []
        for tag in self.submission.tags:
            new_tag_matches = re.search(self.regex, tag)
            if new_tag_matches:
                tag_matches.append((tag, ','.join(new_tag_matches.groups())))

        if description_matches:
            matched_on = ','.join(description_matches.groups())
            match_bundle.add_match(
                FalanoucMatch(match_type='description', matched_on=matched_on, matcher='regex')
            )
            self.logger.debug(
                f'Submission `{self.submission.id}` description matched regex for `{matched_on}`'
            )
        if title_matches:
            matched_on = ','.join(title_matches.groups())
            match_bundle.add_match(
                FalanoucMatch(match_type='title', matched_on=matched_on, matcher='regex')
            )
            self.logger.debug(
                f'Submission `{self.submission.id}` title matched regex for `{matched_on}`'
            )
        if tag_matches:
            for tag_match in tag_matches:
                match_bundle.add_match(
                    FalanoucMatch(match_type='tag', matched_on=tag_match[1], matcher='regex')
                )
                self.logger.debug(
                    f'Submission `{self.submission.id}` tag `{tag_match[0]}` matched regex for '
                    f'`{tag_match[1]}`'
                )
        if match_bundle:
            return match_bundle
        return

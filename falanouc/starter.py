#!/usr/bin/env python
# -*- coding: utf-8 -*-

from falanouc.classes import FalanoucStarter
from falanouc.utils import get_options


def main() -> None:
    options = get_options()
    starter = FalanoucStarter(daemon=options['daemon'], config_file=options['config_file'])
    starter.main()


if __name__ == '__main__':
    main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import logging

from falanouc.version import __version__


def get_options() -> dict:
    parser = argparse.ArgumentParser(
        prog='falanouc',
        description="Monitor FA Artists",
        fromfile_prefix_chars='@'
    )
    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version="%(prog)s {}".format(__version__)
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        dest="debug_logging",
        help="Show debug messages"
    )
    parser.add_argument(
        "-c",
        "--config",
        action="store",
        dest="config_file",
        help="Config file path if not default (`~/.config/falanouc/config.json`)",
        default='~/.config/falanouc/config.json'
    )

    parser.add_argument(
        "-D",
        "--daemon",
        action="store_true",
        dest="daemon",
        help="Run falanouc as a daemon"
    )

    options = vars(parser.parse_args())
    if options['debug_logging']:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)-7s] %(message)s')
    else:
        logging.basicConfig(level=logging.INFO, format='%(message)s')
    del options['debug_logging']

    return options

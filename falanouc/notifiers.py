#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations
from abc import ABC
import os
from typing import Optional, TYPE_CHECKING
import datetime
import pushbullet
import pathlib

from falanouc.mixins import LoggerMixin

if TYPE_CHECKING:
    from falanouc.classes import FalanoucSubmission, FalanoucMatchBundle


class NotifierBase(LoggerMixin, object):
    def main(self, submission: FalanoucSubmission, match_bundle: FalanoucMatchBundle) -> None:
        raise NotImplementedError


class LogfileNotifier(NotifierBase, ABC):
    def __init__(
            self,
            logfile: Optional[str] = './falanouc_notifications.txt',
            format_str: Optional[str] = '[{alert_dt}] `{title}` by {artist} ({submission_link}): '
                                        '{match_bundle}',
            **kwargs
    ) -> None:
        super().__init__(**kwargs)
        self.logfile = str(pathlib.Path(logfile).expanduser().resolve())
        self.format_str = format_str

    def main(self, submission: FalanoucSubmission, match_bundle: FalanoucMatchBundle) -> None:
        with open(self.logfile, 'a') as logfile:
            logfile.write(
                self.format_str.format(
                    log_dt=datetime.datetime.now(),
                    alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
                    submission_id=submission.submission_id,
                    artist=submission.artist,
                    description=submission.description,
                    title=submission.title,
                    tags=submission.tags,
                    rating=submission.rating,
                    submission_link=submission.submission_link,
                    artist_link=submission.artist_link,
                    match_bundle=match_bundle
                ) + os.linesep
            )


class DownloadNotifier(NotifierBase, ABC):
    def __init__(
            self,
            download_path: Optional[str] = './'
    ) -> None:
        self.download_path = download_path

    def main(self, submission: FalanoucSubmission, match_bundle: FalanoucMatchBundle) -> None:
        submission.download_file(file_path=self.download_path)


class EmailNotifier(NotifierBase, ABC):
    pass


class PushBulletNotifier(NotifierBase, ABC):
    def __init__(
            self,
            api_key: str,
            title_format: Optional[str] = '`{title}` by {artist}',
            body_format: Optional[str] = '{match_bundle} - {submission_link}',
            send_as_link: bool = False,
            channel_format: Optional[str] = None,
            channel: Optional[pushbullet.pushbullet.Channel] = None,
            device_format: Optional[str] = None,
            device: Optional[pushbullet.pushbullet.Device] = None,
            email_format: Optional[str] = None,
            chat: Optional[pushbullet.pushbullet.Chat] = None,
    ) -> None:
        self.api_key = api_key
        self.title_format = title_format
        self.body_format = body_format
        self.send_as_link = send_as_link
        self.channel_format = channel_format
        self.channel = channel
        self.device_format = device_format
        self.device = device
        self.email = None
        self.email_format = email_format
        self.chat = chat
        self.pushbullet = pushbullet.Pushbullet(api_key=self.api_key)

    def main(self, submission: FalanoucSubmission, match_bundle: FalanoucMatchBundle) -> None:
        title_str = self.title_format.format(
            log_dt=datetime.datetime.now(),
            alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
            submission_id=submission.submission_id,
            artist=submission.artist,
            description=submission.description,
            title=submission.title,
            tags=submission.tags,
            rating=submission.rating,
            submission_link=submission.submission_link,
            artist_link=submission.artist_link,
            match_bundle=match_bundle
        )
        body_str = self.body_format.format(
            log_dt=datetime.datetime.now(),
            alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
            submission_id=submission.submission_id,
            artist=submission.artist,
            description=submission.description,
            title=submission.title,
            tags=submission.tags,
            rating=submission.rating,
            submission_link=submission.submission_link,
            artist_link=submission.artist_link,
            match_bundle=match_bundle
        )
        if self.channel_format and not self.channel:
            self.channel = self.channel_format.format(
                log_dt=datetime.datetime.now(),
                alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
                submission_id=submission.submission_id,
                artist=submission.artist,
                description=submission.description,
                title=submission.title,
                tags=submission.tags,
                rating=submission.rating,
                submission_link=submission.submission_link,
                artist_link=submission.artist_link,
                match_bundle=match_bundle
            )
            try:
                self.channel = self.pushbullet.get_channel(self.channel)
            except pushbullet.errors.PushbulletError:
                self.logger.warning(f'Could not fine Pushbullet channel `{self.channel}`, '
                                    f'excluding.')
                self.channel = None
        if self.device_format and not self.device:
            self.device = self.device_format.format(
                log_dt=datetime.datetime.now(),
                alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
                submission_id=submission.submission_id,
                artist=submission.artist,
                description=submission.description,
                title=submission.title,
                tags=submission.tags,
                rating=submission.rating,
                submission_link=submission.submission_link,
                artist_link=submission.artist_link,
                match_bundle=match_bundle
            )
            try:
                self.device = self.pushbullet.get_device(self.device)
            except pushbullet.errors.PushbulletError:
                self.logger.warning(f'Could not fine Pushbullet device `{self.device}`, '
                                    f'excluding.')
                self.device = None
        if self.email_format:
            self.email = self.email_format.format(
                log_dt=datetime.datetime.now(),
                alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
                submission_id=submission.submission_id,
                artist=submission.artist,
                description=submission.description,
                title=submission.title,
                tags=submission.tags,
                rating=submission.rating,
                submission_link=submission.submission_link,
                artist_link=submission.artist_link,
                match_bundle=match_bundle
            )
        if self.send_as_link:
            self.pushbullet.push_link(
                title=title_str,
                body=body_str,
                url=submission.submission_link,
                device=self.device,
                chat=self.chat,
                email=self.email,
                channel=self.channel
            )
        else:
            self.pushbullet.push_note(
                title=title_str,
                body=body_str,
                device=self.device,
                chat=self.chat,
                email=self.email,
                channel=self.channel
            )


class PopupNotifier(NotifierBase, ABC):
    def __init__(self) -> None:
        pass

    def main(self, submission: FalanoucSubmission, match_bundle: FalanoucMatchBundle) -> None:
        from tkinter import Tk, Frame, Button, Label, CENTER
        import webbrowser
        root = Tk()
        root.title("Falanouc")
        root.configure(bg='#3E4149')
        root.minsize(250, 125)
        root.geometry("300x150")
        root.attributes("-topmost", True)
        frame = Frame(root)
        frame.pack()
        sub_url = submission.submission_link

        def open_url(url):
            webbrowser.open_new(url)
            root.destroy()

        Label(
            root,
            justify=CENTER,
            compound=CENTER,
            padx=10,
            text=f"New submission by {submission.artist} found!\n"
                 f"{submission.title}\n"
                 f"{match_bundle}"
        ).pack()
        Button(
            root,
            text="View Submission",
            command=lambda aurl=sub_url: open_url(aurl),
            highlightbackground='#3E4149'
        ).pack()
        Button(
            root, text="Ignore", command=root.destroy, highlightbackground='#3E4149'
        ).pack()
        root.mainloop()


class DiscordWebHookNotifier(NotifierBase, ABC):
    def __init__(
            self,
            webhook_url: str
    ) -> None:
        self.webhook_url = webhook_url

    def main(self, submission: FalanoucSubmission, match_bundle: FalanoucMatchBundle) -> None:
        try:
            from discord_webhook import DiscordWebhook, DiscordEmbed
        except ImportError:
            self.logger.warn('Could not import `discord_webhook`! Please install before using'
                             'this notifier. Skipping.')
            return
        import re
        artist_icon = 'https://a.facdn.net/1424255659/username.gif'
        try:
            artist_page = submission.api.get(f'/user/{submission.artist}')
            artist_icon = 'https:{}'.format(
                re.findall(
                    r'user-nav-avatar" alt="[^"]*" src="([^"]*)"', str(artist_page.content)
                )[0]
            )
        except Exception as e:
            self.logger.warning(f'Encountered error when trying to download user icon\n{e}')

        webhook = DiscordWebhook(url=self.webhook_url)
        embed = DiscordEmbed(
            title=submission.title,
            description=str(match_bundle),
            color=242424
        )
        embed.set_author(
            name=submission.artist,
            url=submission.artist_link,
            icon_url=artist_icon
        )
        embed.set_image(url=submission.file_url)
        embed.set_timestamp()
        embed.add_embed_field(name='Submission Link', value=submission.submission_link)
        webhook.add_embed(embed)
        webhook.execute()


class MacPopupNotifier(NotifierBase, ABC):
    def __init__(
            self,
            notification_format: str = '',
            title_format: Optional[str] = None,
            subtitle_format: Optional[str] = None,
            sound: Optional[str] = None
    ) -> None:
        self.notification_format = notification_format
        self.title_format = title_format
        self.subtitle_format = subtitle_format
        self.sound = sound

    def main(self, submission: FalanoucSubmission, match_bundle: FalanoucMatchBundle) -> None:
        osascript_command = "osascript -e 'display notification \""
        notification_text = self.notification_format.format(
            log_dt=datetime.datetime.now(),
            alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
            submission_id=submission.submission_id,
            artist=submission.artist,
            description=submission.description,
            title=submission.title,
            tags=submission.tags,
            rating=submission.rating,
            submission_link=submission.submission_link,
            artist_link=submission.artist_link,
            match_bundle=match_bundle
        )
        osascript_command += notification_text + "\""
        if self.title_format:
            title_text = self.title_format.format(
                log_dt=datetime.datetime.now(),
                alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
                submission_id=submission.submission_id,
                artist=submission.artist,
                description=submission.description,
                title=submission.title,
                tags=submission.tags,
                rating=submission.rating,
                submission_link=submission.submission_link,
                artist_link=submission.artist_link,
                match_bundle=match_bundle
            )
            osascript_command += " with title \""
            osascript_command += title_text
            osascript_command += "\""
        if self.subtitle_format:
            subtitle_text = self.subtitle_format.format(
                log_dt=datetime.datetime.now(),
                alert_dt=datetime.datetime.fromtimestamp(submission.alert_dt),
                submission_id=submission.submission_id,
                artist=submission.artist,
                description=submission.description,
                title=submission.title,
                tags=submission.tags,
                rating=submission.rating,
                submission_link=submission.submission_link,
                artist_link=submission.artist_link,
                match_bundle=match_bundle
            )
            osascript_command += " subtitle \""
            osascript_command += subtitle_text
            osascript_command += "\""
        if self.sound:
            osascript_command += f" sound name \"{self.sound}\""
        osascript_command += "'"
        os.system(osascript_command)

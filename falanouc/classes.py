#!/usr/bin/env python
# -*- coding: utf-8 -*-
import faapi
import json
import pathlib
import queue
import signal
import sqlite3
import time
# noinspection PyProtectedMember
from pydoc import locate as class_locate
from typing import Any, List, Optional, Dict, Union, Iterable

from falanouc.mixins import DBAwareMixin, LoggerMixin, SDNotifyMixin
from falanouc.notifiers import NotifierBase
from falanouc.matchers import FalanoucMatch, FalanoucMatchBundle, FalanoucRegexMatcher, \
    FalanoucTextMatcher


class FalanoucSubmission(DBAwareMixin, object):
    DB_TABLE = 'submissions'
    DB_ID = 'id'
    DB_MAPPING = {
        'submission_id': DBAwareMixin.DBColumn(attribute='submission_id', column='id'),
        'artist': DBAwareMixin.DBColumn(attribute='artist', column='artist'),
        'alert_dt': DBAwareMixin.DBColumn(attribute='alert_dt', column='alert_dt')
    }

    def __init__(
            self,
            submission_id: int,
            artist: str,
            api: Optional[faapi.FAAPI] = None,
            rating: Optional[str] = None,
            title: Optional[str] = None,
            description: Optional[str] = None,
            tags: Optional[List[str]] = None,
            alert_dt: Optional[int] = None,
            file_url: Optional[str] = None,
            **kwargs
    ) -> None:
        super().__init__(**kwargs)
        self.submission_id = submission_id
        self._SELF_ID = 'submission_id'
        self.artist = artist
        self.api = api
        self.rating = rating
        self.title = title
        self.description = description
        if not tags:
            tags = []
        self.tags = tags
        self.alert_dt = alert_dt
        self.file_url = file_url

    @property
    def submission_link(self) -> str:
        return f'https://www.furaffinity.net/view/{self.submission_id}/'

    @property
    def artist_link(self) -> str:
        return f'https://www.furaffinity.net/user/{self.artist}/'

    def download_file(self, file_path: str = './') -> None:
        sub, sub_file = self.api.get_sub(self.submission_id, get_file=True)
        output_path = pathlib.Path(file_path).expanduser().resolve() / sub.file_url.split("/")[-1]
        with open(output_path, "wb") as f:
            f.write(sub_file)

    @classmethod
    def from_faapi_submission(
            cls,
            submission: Union[faapi.Submission, faapi.SubmissionPartial],
            api: faapi.FAAPI,
            db: sqlite3.Connection
    ) -> 'FalanoucSubmission':
        if isinstance(submission, faapi.SubmissionPartial):
            submission = api.get_sub(submission.id)[0]
        return cls(
            submission_id=submission.id,
            artist=submission.author,
            api=api,
            rating=submission.rating,
            title=submission.title,
            description=submission.description,
            tags=submission.tags,
            db=db,
            alert_dt=int(time.time()),
            file_url=submission.file_url
        )

    def save(self) -> None:
        self._db_push()


class FalanoucConfig(object):
    """
    Configuration abstraction for Falanouc.

    For defaults, see the docstring for the specific configuration classes below, or see
    config.json.default

    Argumants:

    db_path:        Sqlite database path
    idle_timer:     Seconds between runs
    cookie_a:       Cookie 'a' from an active FA session
    cookie_b:       Cookie 'b' from an active FA session
    artists:        dictionary of artists to watch, and strings to watch for
    notifiers:      list of notifier classes and arguments to handle matches
    """
    def __init__(
            self,
            config_file: str,
            db_path: str = '~/.config/falanouc/falanouc.db',
            idle_timer: int = 1800,
            cookie_a: Optional[str] = None,
            cookie_b: Optional[str] = None,
            artists: Optional[dict] = None,
            notifiers: Optional[Dict[str, Union[List[Any], Dict[str, Any]]]] = None,
            **kwargs
    ) -> None:
        self.config_file = str(pathlib.Path(config_file).expanduser().resolve())
        self.db_path = str(pathlib.Path(db_path).expanduser().resolve())
        self.idle_timer = idle_timer
        self.cookies = []
        if cookie_a and cookie_b:
            self.cookies.extend([
                {"name": "a", "value": cookie_a},
                {"name": "b", "value": cookie_b}
            ])
        if not artists:
            artists = dict()
        self.artists = artists
        if not notifiers:
            notifiers = []
        self.notifiers = notifiers

    @classmethod
    def from_json(cls, config_file) -> 'FalanoucConfig':
        config_file = str(pathlib.Path(config_file).expanduser().resolve())
        with open(config_file, 'r') as fp:
            config = json.load(fp=fp)
        config['config_file'] = config_file
        return cls(**config)


class FalanoucDaemon(LoggerMixin, SDNotifyMixin, object):
    """ Daemon for Falanouc """
    def __init__(
            self,
            config: FalanoucConfig,
            daemon_queue: queue.Queue = None,
            logging_queue: queue.Queue = None,
            submission_notifiers: Optional[Iterable[NotifierBase]] = None
    ) -> None:
        self.config = config
        self.daemon_queue = daemon_queue
        self.logging_queue = logging_queue
        self.submission_notifiers = submission_notifiers
        self.runner = None
        self._old_config = None

    def main(self) -> None:
        self.logger.info('Running as a daemon')
        self._sd_message(b'READY=1')
        self.runner = FalanoucRunner(
            config=self.config,
            submission_notifiers=self.submission_notifiers
        )
        while True:
            try:
                self.runner.main()
                self.logger.debug(f'Sleeping for {self.runner.config.idle_timer} seconds')
                time.sleep(self.runner.config.idle_timer)
            except Exception as e:
                self.logger.critical(e)
                self.runner.shutdown()
                self._sd_message(b'STOPPING=1')
                exit(1)

    def hup(
            self,
            signal_number=None,
            frame=None,
            config_file: Optional[str] = None,
            *args,
            **kwargs
    ) -> None:
        if signal_number:
            self.logger.debug('Daemon received SIGHUP')
            self._sd_message(b'RELOADING=1')
        self._old_config = self.config
        del self.config
        try:
            if config_file:
                self.config = FalanoucConfig.from_json(config_file)
            else:
                self.config = FalanoucConfig.from_json(self._old_config.config_file)
        except Exception as e:
            self.logger.warning(
                f'Could not replace running config! Continuing with old config.\n{e}'
            )
            del self.config
            self.config = self._old_config
        else:
            self.runner.hup(signal_number, frame, config_file=config_file, *args, **kwargs)
        finally:
            del self._old_config
        self.logger.info('Daemon reload successful')
        self._sd_message(b'READY=1')


class FalanoucRunner(LoggerMixin, SDNotifyMixin, object):
    """ Heavy lifting """
    def __init__(
            self,
            config: FalanoucConfig,
            submission_notifiers: Optional[Iterable[NotifierBase]] = None
    ) -> None:
        self.config = config
        self.api = None
        self.db_handle = None
        self.submissions = []
        self.submissions_by_id = {}
        self.submissions_by_artist = {}
        self.submission_notifiers = submission_notifiers
        self._old_config = None
        self._prep_done = False

    def prep(self) -> None:
        self.api = faapi.FAAPI(self.config.cookies)
        self.db_handle = sqlite3.connect(self.config.db_path)
        self.create_database()
        self.logger.info('Loading previously seen submissions from the database')
        all_submissions = self.db_handle.execute('select id from submissions')
        for submission in all_submissions:
            self.add_submission(FalanoucSubmission.from_db_id(self.db_handle, submission[0]))
        self._prep_done = True

    def main(self) -> None:
        if not self._prep_done:
            self.prep()
        self.logger.info('Checking for new submissions')
        artist_number = 1
        submission_matches = 0
        for artist in self.config.artists:
            self.logger.info(f'Checking `{artist}` ({artist_number}/{len(self.config.artists)})')
            artist_number += 1
            artist_status = self.api.user_exists(artist)
            self.logger.debug(f'Artist `{artist}` : `{artist_status}`')
            if artist_status == 3:
                self.logger.critical(
                    f'An unknown error occurred! Are `cookie_a` and `cookie_b` valid?'
                )
            gallery = self.api.gallery(artist)
            for submission in gallery[0]:
                if submission.id not in self.submissions_by_id:
                    submission_matches += 1
                    self.handle_new_submission(submission)
        self.logger.info(f'Found {submission_matches} new submissions.')

    def shutdown(self) -> None:
        self._prep_done = False
        self.db_handle.close()

    def add_submission(self, submission: FalanoucSubmission) -> None:
        if submission not in self.submissions:
            self.submissions.append(submission)
        if submission.submission_id not in self.submissions_by_id:
            self.submissions_by_id[submission.submission_id] = submission
        if submission.artist not in self.submissions_by_artist:
            self.submissions_by_artist[submission.artist] = []
        if submission not in self.submissions_by_artist[submission.artist]:
            self.submissions_by_artist[submission.artist].append(submission)

    def create_database(self) -> None:
        self.db_handle.execute(
            'CREATE TABLE IF NOT EXISTS submissions('
            '    id   INT PRIMARY KEY,'
            '    artist          TEXT NOT NULL,'
            '    alert_dt        INT'
            ')'
        )

    def hup(self, signal_number=None, frame=None, config_file: Optional[str] = None) -> None:
        if signal_number:
            self.logger.debug('Runner received SIGHUP')
        if config_file:
            self.logger.info(f'Received request to swap running config to `{config_file}`')
        else:
            self.logger.info(f'Received request to reload current config')
        self._old_config = self.config
        del self.config
        try:
            if config_file:
                self.config = FalanoucConfig.from_json(config_file)
            else:
                self.config = FalanoucConfig.from_json(self._old_config.config_file)
        except Exception as e:
            self.logger.warning(f'Could not replace running config! Continuing with old config.\n{e}')
            del self.config
            self.config = self._old_config
        else:
            self.api = faapi.FAAPI(self.config.cookies)
            self.shutdown()
            self.db_handle = sqlite3.connect(self.config.db_path)
        finally:
            del self._old_config
        self.logger.info('Runner reload successful')

    def handle_new_submission(self, submission: faapi.SubmissionPartial) -> None:
        assert self.api.sub_exists(submission.id) == 0
        full_submission = self.api.get_sub(submission.id)[0]

        match_bundle = FalanoucMatchBundle()
        if 'submissions' in self.config.artists[submission.author.lower()]:
            if 'text' in self.config.artists[submission.author.lower()]['submissions']:
                for search_key in self.config.artists[submission.author.lower()]['submissions']['text']:
                    new_matches = FalanoucTextMatcher(
                        submission=full_submission,
                        search_key=search_key
                    ).matches()
                    if new_matches:
                        match_bundle = match_bundle + new_matches
            if 'regex' in self.config.artists[submission.author.lower()]['submissions']:
                for search_key in self.config.artists[submission.author.lower()]['submissions']['regex']:
                    new_matches = FalanoucRegexMatcher(
                        submission=full_submission,
                        search_key=search_key
                    ).matches()
                    if new_matches:
                        match_bundle = match_bundle + new_matches
        submission = FalanoucSubmission.from_faapi_submission(
            submission=full_submission,
            api=self.api,
            db=self.db_handle
        )
        if match_bundle:
            self.submission_match(submission, match_bundle)
        self.add_submission(submission)
        submission.save()

    def submission_match(
            self,
            submission: FalanoucSubmission,
            match_bundle: FalanoucMatchBundle
    ) -> None:
        self.logger.info(f'Found matching submission `{submission.submission_id}`')
        for notifier in self.submission_notifiers:
            notifier.main(submission, match_bundle)


class FalanoucStarter(LoggerMixin, SDNotifyMixin, object):
    """ Falanouc entry point """
    def __init__(
            self,
            daemon: bool = True,
            config_file: str = '~/.config/falanouc/config.json'
    ) -> None:
        self.daemon = daemon
        self.config_file = str(pathlib.Path(config_file).expanduser().resolve())
        try:
            self.config = FalanoucConfig.from_json(self.config_file)
        except FileNotFoundError:
            self.logger.info('No config file available, copying example config to default location')
            self.first_start()
        self.check_sane()

    def main(self) -> None:
        self.logger.info('Falanouc started')
        self._sd_message(b'STATUS=Started Falanouc')
        notifiers = []
        for notifier in self.config.notifiers:
            try:
                notifier_class = class_locate(notifier)
            except ImportError:
                self.logger.warning(f'Could not load notifier `{notifier}`! Continuing without.')
            else:
                self.logger.debug(f'Loading notifier `{notifier}`')
                if isinstance(self.config.notifiers[notifier], list):
                    notifiers.append(notifier_class(*self.config.notifiers[notifier]))
                elif isinstance(self.config.notifiers[notifier], dict):
                    notifiers.append(notifier_class(**self.config.notifiers[notifier]))
                else:
                    notifiers.append(notifier_class(self.config.notifiers[notifier]))
        if self.daemon:
            self.logger.info('Running as a daemon')
            falanouc = FalanoucDaemon(config=self.config, submission_notifiers=notifiers)
        else:
            falanouc = FalanoucRunner(config=self.config, submission_notifiers=notifiers)
        signal.signal(signal.SIGHUP, falanouc.hup)
        falanouc.main()

    # noinspection PyTypeChecker
    def first_start(self) -> None:
        import importlib.resources as pkg_resources
        import shutil
        config_path = pathlib.Path(self.config_file).expanduser().resolve()
        config_dir = config_path.parent
        config_dir.mkdir(parents=True, exist_ok=True)
        from . import data
        self.logger.debug(f'Copying example config file to `{config_path}`')
        with pkg_resources.path(data, 'config.json.example') as example_config:
            shutil.copy(example_config, config_path)
        print(
            f'\n'
            f'====================================================\n'
            f'| Before running falanouc again, be sure\n'
            f'| to go set up the configuration at:\n'
            f'| `{config_path}`\n'
            f'====================================================\n'
            f'\n'
        )
        exit(0)

    def check_sane(self) -> None:
        try:
            with open(self.config.db_path, "r"):
                pass
        except FileNotFoundError:
            self.logger.info(
                f'Database file `{self.config.db_path}` does not exist and will be created'
            )
        try:
            test_db = sqlite3.connect(self.config.db_path)
        except Exception as e:
            self.logger.critical(f'Database file could not be created!\n{e}')
            exit(1)
        else:
            test_db.close()


"""
class FalanoucThreadManager(LoggerMixin, object):
    def __init__(self, **kwargs):
        self.config_obj = FalanoucConfig.from_json(kwargs.get('config_file', 'config.json'))
        self.logging_queue = queue.Queue()
        self.config_obj.logging['formatters']['web_brief'] = {
            "class": "logging.Formatter",
            "datefmt": "%Y%m%d %H:%M:%S",
            "format": "%(asctime)s [%(levelname)-7s] %(message)s\r\n"
        }
        self.config_obj.logging['handlers']['list_queue'] = {
            'class': 'classes.QueuingHandler',
            'formatter': 'web_brief',
            'level': 15,
            'message_queue': self.logging_queue
        }
        self.config_obj.logging['root']['handlers'].append('list_queue')
        logging.config.dictConfig(self.config_obj.logging)
        self.daemon_queue = queue.Queue(5)
        self.to_api_queue = queue.Queue(5)
        self.daemon = FalanoucDaemon(
            root_config=self.config_obj,
            daemon_queue=self.daemon_queue,
            logging_queue=self.logging_queue
        )
        self.daemon_thread = threading.Thread(
            target=self.daemon.main,
            name='daemon'
        )

    def run(self):
        self.daemon_thread.start()
"""

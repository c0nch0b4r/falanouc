#!/usr/bin/env python
# -*- coding: utf-8 -*-
import inspect
import logging
import sqlite3
import sys
from typing import Union, Optional


class LoggerMixin(object):
    @property
    def logger(self):
        if not hasattr(self, '_logger') or not self._logger:
            stack = inspect.stack()
            name = __name__
            if "self" in stack[1][0].f_locals:
                name = '{module}.{class_name}'.format(
                    module=stack[1][0].f_locals["self"].__module__,
                    class_name=stack[1][0].f_locals["self"].__class__.__name__
                )
            elif "__name__" in stack[1][0].f_locals and stack[1][0].f_code.co_name == '<module>':
                name = stack[1][0].f_locals['__name__']
            logger = logging.getLogger(name)
            if not len(logger.handlers):
                pass
            self._logger = logger
        return self._logger


class SDNotifyMixin(object):
    def _sd_message(self, message: bytes) -> None:
        if not hasattr(self, '_sd_notifier'):
            if 'sd_notify' not in sys.modules:
                try:
                    import sd_notify
                except ImportError:
                    self._sd_notifier = None
                else:
                    self._sd_notifier = sd_notify.Notifier
        if self._sd_notifier and self._sd_notifier.enabled():
            self._sd_notifier._send(message)


class DBAwareMixin(object):
    DB_TABLE = None
    DB_ID = None
    DB_MAPPING = None

    class DBColumn(object):
        def __init__(
                self,
                attribute: str,
                column: str,
                col_type: str = 'TEXT',
                allow_null: bool = True,
                default_value: Union[None, str, int, bytes] = None,
                primary_key: bool = False,
                auto_increment: bool = False,
                extra: Optional[str] = None,
                **kwargs
        ) -> None:
            self.attribute = attribute
            self.column = column
            assert col_type in ['NULL', 'INT', 'INTEGER', 'NUMERIC', 'BOOLEAN', 'DATE', 'DATETIME',
                                'REAL', 'DOUBLE', 'FLOAT', 'TEXT', 'CLOB', 'BLOB']
            self.type = col_type
            self.allow_null = allow_null
            self.default_value = default_value
            self.primary_key = primary_key
            self.auto_increment = auto_increment
            self.extra = extra

        def __str__(self) -> str:
            primary_key = ''
            auto_increment = ''
            allow_null = ''
            default_value = ''
            extra = ''
            if self.primary_key:
                primary_key = ' PRIMARY KEY'
            if self.auto_increment:
                auto_increment = ' AUTOINCREMENT'
            if not self.allow_null:
                allow_null = ' NOT NULL'
            if self.default_value:
                default_value = f' DEFAULT {self.default_value}'
            if self.extra:
                extra = f' {self.extra}'
            return f'{self.column} {self.type}{primary_key}{auto_increment}{allow_null}{default_value}{extra}'

    def __init__(self, **kwargs) -> None:
        self._db = kwargs.get('db')
        self._SELF_ID = None

    @property
    def SELF_ID(self) -> str:
        if hasattr(self, '_SELF_ID'):
            return self._SELF_ID
        for row in self.DB_MAPPING:
            if self.DB_MAPPING[row].column == self.DB_ID:
                self._SELF_ID = self.DB_MAPPING[row].attribute
        return self._SELF_ID

    @classmethod
    def from_db_id(cls, db_con, id_value, extra=None) -> 'DBAwareMixin':
        if not extra:
            extra = {}
        db_con.row_factory = sqlite3.Row
        cursor = db_con.cursor()
        cursor.execute(
            f'SELECT * FROM {cls.DB_TABLE} WHERE {cls.DB_ID}=?',
            (id_value,)
        )
        table_row = cursor.fetchone()
        init_data = {}
        for row_key in table_row.keys():
            for prop in cls.DB_MAPPING:
                if cls.DB_MAPPING[prop].column == row_key:
                    init_data[prop] = table_row[row_key]
        final_data = {**init_data, **extra}
        return cls(**final_data)

    def _db_create(self) -> None:
        cursor = self._db.cursor()
        create_statement = f'CREATE TABLE IF NOT EXISTS {self.DB_TABLE}('
        columns = []
        for column in self.DB_MAPPING:
            columns.append(str(self.DB_MAPPING[column]))
        create_statement += ', '.join(columns)
        create_statement += ')'
        cursor.execute(create_statement)
        self._db.commit()

    def _db_push(self) -> None:
        columns = []
        value_map = {}
        cursor = self._db.cursor()
        if not self.__dict__[self.SELF_ID]:
            self.__dict__[self.SELF_ID] = cursor.lastrowid
        db_items = self.__dict__.copy()
        for prop in db_items:
            if prop in self.DB_MAPPING:
                columns.append(self.DB_MAPPING[prop].column)
                value_map[self.DB_MAPPING[prop].column] = db_items[prop]
        if len(columns) > 0:
            statement = f'INSERT OR REPLACE INTO {self.DB_TABLE}({",".join(columns)}) VALUES (:{",:".join(columns)})'
            cursor.execute(statement, value_map)
        self._db.commit()

    def _db_pull(self) -> None:
        self._db.row_factory = sqlite3.Row
        cursor = self._db.cursor()
        cursor.execute(
            f'SELECT * FROM {self.DB_TABLE} WHERE {self.DB_ID}=?',
            (getattr(self, self.SELF_ID),)
        )
        table_row = cursor.fetchone()
        for row_key in table_row.keys():
            for prop in self.DB_MAPPING:
                if self.DB_MAPPING[prop].column == row_key:
                    setattr(self, prop, table_row[row_key])

    def _db_delete(self) -> bool:
        cursor = self._db.cursor()
        cursor.execute(
            f'DELETE FROM {self.DB_TABLE} WHERE {self.DB_ID}=?',
            (getattr(self, self.SELF_ID),)
        )
        cursor.execute(
            f'SELECT * FROM {self.DB_TABLE} WHERE {self.DB_ID}=?',
            (getattr(self, self.SELF_ID),)
        )
        row = cursor.fetchone()
        if len(row) >= 1:
            return False
        else:
            return True



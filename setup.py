#!/usr/bin/var python
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


try:
    from falanouc.version import __version__
except ImportError:
    pass

exec(open('falanouc/version.py').read())


setup(
    name='falanouc',
    version=__version__,
    description='A FurAffinity monitoring tool',
    long_description=readme(),
    long_description_content_type='text/x-rst',
    author='c0nch0b4r',
    author_email='lp1.on.fire@gmail.com',
    packages=[
        'falanouc'
    ],
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8'
    ],
    keywords='furaffinity',
    url='https://bitbucket.org/c0nch0b4r/falanouc',
    download_url='https://bitbucket.org/c0nch0b4r/falanouc/get/' + __version__ + '.tar.gz',
    project_urls={
        'Source': 'https://bitbucket.org/c0nch0b4r/falanouc/src'
    },
    python_requires='>=3.6, <4',
    install_requires=[
        'typing',
        'faapi',
        'pushbullet.py'
    ],
    extras_require={
        'systemd': ['sd-notify']
    },
    entry_points={
        'console_scripts': ['falanouc=falanouc.starter:main'],
    },
    include_package_data=True
)
